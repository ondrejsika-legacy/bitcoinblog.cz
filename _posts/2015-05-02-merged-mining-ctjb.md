---
layout: post
title: Merged Mining (CTJB)
language: CZ
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

Mel jsem na [CTJB 2015](http://ctjb.net/2015) prednasku o Merged Miningu.

Slides jsou zde <https://speakerdeck.com/ondrejsika/merged-mining-ctjb>

