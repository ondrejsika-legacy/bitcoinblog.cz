---
layout: post
title: Namecoin nodes
language: EN
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

List of good available namecoin nodes:

    # US
    50.31.189.41
    76.74.177.197
    96.44.183.195
    204.27.61.162

    # DE
    85.25.213.26
    92.51.148.91

    # FR
    188.165.254.191
    212.47.228.136

    # RU
    91.214.70.203

    # CN
    118.244.207.8
    173.254.198.163

    # JP
    106.186.123.180

Data from <https://bitinfocharts.com/namecoin/nodes/>

