---
layout: post
title: Namecoin.cz
language: CS
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

Dnes jsem zalozil [namecoin.cz](http://namecoin.cz), zatim na nem provozuji verene Namecoin uzle livenet (__live01.namecoin.cz__) i testnet (__test01.namecoin.cz__). Vsechny nody maji otevrene RPC (usename: __nmcrpc__, password: __nmc__).

RPC muzete vyzkouset prez curl:

    curl -d '{"jsonrpc":"1.0","id":"curltext","method":"getinfo","params":[]}' 'nmcrpc:nmc@live01.namecoin.cz:8336'

Provozuji take regtest node (regtest je velmi nizka difficulty pro testovani miningu) na __reg01.namecoin.cz__. I ten ma otevrene RPC, ovsem bezi na __28336__, protoze defaultni port je stejny jako u testnetu.

