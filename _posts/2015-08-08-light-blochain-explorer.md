---
layout: post
title: Light Blockchain Explorer
language: EN
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

When I tested merged mining and lot stuff on regtest, I needed simple blochain explorer for my chains and coins.

I looked for existing, but every explorers are hard to use or config.

That's a reason, why I created Light Blockhain Explorer. It need only 4 generic Xcoind RPC calls. This results to easy to best use friendly block explorer.

Here is code on github: <https://github.com/ondrejsika/lbe>

